'use strict'

let path = require('path')
let webpack = require('webpack')
let baseConfig = require('./base')
let defaultSettings = require('./defaults')
let DashboardPlugin = require('webpack-dashboard/plugin')

/**
 * Project configuration: A JSON file that contains all the meta data used
 * to generate the index.html. Needs to be adapted for every project.
 */
let projectConfig = require('../src/config.json')

// Add needed plugins here
let HtmlWebpackPlugin = require('html-webpack-plugin')

let config = Object.assign({}, baseConfig, {
  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:' + defaultSettings.port,
    'webpack/hot/only-dev-server',
    './src/index'
  ],
  output: {
    path: path.join(__dirname, '/../dist/assets'),
    filename: 'app.js',
    publicPath: defaultSettings.devPublicPath
  },
  cache: true,
  devtool: 'inline-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.LoaderOptionsPlugin({
      debug: true
    }),
    new DashboardPlugin()
  ],
  module: defaultSettings.getDefaultModules()
})

// Add needed loaders to the defaults here
config.module.rules.push({
  test: /\.(js|jsx)$/,
  loader: 'babel-loader',
  include: [].concat(
    [path.join(__dirname, '/../src')]
  )
})

config.plugins = config.plugins.concat(defaultSettings.getDefaultPlugins())

module.exports = config
