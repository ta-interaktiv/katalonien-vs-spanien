// @flow
import { createAction } from 'redux-actions'

export const dumpRawData = createAction('DUMP_RAW_DATA')
export const saveAvailableIncomeBrackets = createAction('SAVE_AVAILABLE_INCOME_BRACKETS')
export const saveOverallExtent = createAction('SAVE_OVERALL_EXTENT')
