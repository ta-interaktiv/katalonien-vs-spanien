import { createActions } from 'redux-actions'
export const {showMoreLines, showLessLines} = createActions({
  SHOW_MORE_LINES: amount => ({amount}),
  SHOW_LESS_LINES: amount => ({amount: -amount})
})
export const {lookupString, flipSortOrder} = createActions('LOOKUP_STRING', 'FLIP_SORT_ORDER')
