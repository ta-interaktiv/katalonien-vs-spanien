/**
 * Created by km on 2017-03-02.
 */
import React from 'react'
require('ta-semantic-ui/semantic/dist/components/button.css')
require('ta-semantic-ui/semantic/dist/components/label.css')

/**
 * Very simple audio player component. Shows a play button that starts and
 * pauses the audio.
 *
 * @param {string} props.source The source URL of the audio file to play.
 * @param {string} props.audioTitle The name of the audio.
 */
export default class AudioPlayer extends React.PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      isPlaying: false
    }

    // Set up dataLayer for event tracking
    // This assumes we have GTM set up, otherwise not much will happen.
    window.dataLayer = window.dataLayer || []

    this.togglePlayStatus = this.togglePlayStatus.bind(this)
    this.playbackEndedHandler = this.playbackEndedHandler.bind(this)
  }

  render () {
    return (
      <div className='audioplayer'>
        <audio onEnded={this.playbackEndedHandler} src={this.props.source} ref={(element) => {
          this.player = element
        }} />
        <button onClick={this.togglePlayStatus} className='ui medium circular red icon button' title={this.props.audioTitle}>
          <i className={`${this.state.isPlaying ? 'pause ' : 'play '}icon`} />
        </button>
      </div>
    )
  }

  togglePlayStatus (event) {
    this.setState({
      isPlaying: !this.state.isPlaying
    })

    if (this.state.isPlaying) {
      this.player.pause()
    } else {
      this.player.play()
      window.dataLayer.push({
        'event': 'Audio Interaktiv',
        'action': 'Play',
        'videoURL': window.location.href,
        'videoTitle': this.props.audioTitle
      })
    }
  }

  playbackEndedHandler (event) {
    this.setState({
      isPlaying: false
    })
  }
}

AudioPlayer.propTypes = {
  source: React.PropTypes.string.isRequired,
  audioTitle: React.PropTypes.string.isRequired
}

AudioPlayer.defaultProps = {

}
