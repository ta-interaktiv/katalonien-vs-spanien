// @flow
import { formatLocale, precisionFixed } from 'd3-format'
// Settings configured here will be merged into the final config object.

const deChLocale = formatLocale(require('de-ch-locale'))
const precision = precisionFixed(0.01)
const formatAsDecimal: (number) => string = deChLocale.format(',.' + precision + 'f')
const formatAsCurrency: (number) => string = deChLocale.format('$,.2f')
const formatAsIntegerCurrency: (number) => string = deChLocale.format('$,')
const formatAsPercent: (number) => string = deChLocale.format('.1%')
/**
 * Formats a number as an integer
 * @param {number}
 * @returns {string}
 */
const formatAsInteger: (number) => string = deChLocale.format(',d')

export default {
  colorScale: ['#276419', '#EEEEEE', '#8E0152'],
  locale: deChLocale,
  formatAsDecimal,
  formatAsCurrency,
  formatAsPercent,
  formatAsInteger,
  formatAsIntegerCurrency
}
